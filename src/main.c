#include <yemu/common.h>
#include <yemu/6502.h>
#include <yemu/ocpu.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>

void usage() {
  printf("%s - Yet another EMUlator\n", "yemu");
  printf("Usage: %s [OPTIONS] [--] filename\n", "yemu");
  printf("       %s --help\n", "yemu");
  printf("Options:\n");
  printf("\n");
  printf("       --help                 display this message\n");
  printf("       --system system_name   choose one of supported systems\n");
}

int main(int argc, char *argv[]) {
  char *infilename = NULL;
  char *system_name = NULL;
  
  if (argc == 1) {
    usage();
    return 0;
  } else if (argc >= 2) {
    for (int i = 1; i <= argc-1; i++) {
      if (argv[i][0] == '-') {
	argv[i]++;
	if(argv[i][0] == '-')
	  argv[i]++;
	if (!strcmp(argv[i], "system")) {
	  system_name = argv[i+1];
	  i+=2;
	} else if (!strcmp(argv[i], "help")) {
	  usage();
	  return 0;
	} else {
	  printf("Unknown argument: %s\n", argv[i]);
	  return 1;
	}
      } else {
	infilename = argv[i];
      }
    }
  }
  
  if (infilename == NULL) {
    printf("%s: no input file specified\n", "yemu");
    return 1;
  }
  
  FILE *infile = fopen(infilename, "rb");
  
  if (infile == NULL) {
    perror("yemu");
    return 1;
  }

  if (system_name == NULL) {
    printf("%s: no system specified\n", "yemu");
    return 1;
  } else if (!strcmp(system_name, "6502")) {
    init6502(infile);
  } else if (!strcmp(system_name, "ocpu")) {
    initOCPU(infile);
  } else {
    printf("%s: not supported system\n", "yemu");
    return 1;
  }
  
  fclose(infile);
  
  return 0;
}
