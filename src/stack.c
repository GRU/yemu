#include <yemu/stack.h>
#include <yemu/common.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

extern stack_t *stack;
extern unsigned int stack_size;
extern byte *sp;

int get_last_item_from_stack() {
  if ((int)(*sp) <= 0) {
    printf("Stack Underflow\n");
    return -1;
  }
  return stack[(*sp)];
}

void push(int value) {
  if (*sp >= stack_size-1) {
    printf("Stack Overflow\n");
  } else {
    (*sp)++;
    stack[*sp] = value;
  }
}

int pop() {
  int value = -1;
  if ((int)(*sp) <= 0) {
    printf("Stack Underflow\n");
  } else {
    value = stack[*sp];
    (*sp)--;
  }
  return value;
}
