#ifndef YEMU_STACK_H
#define YEMU_STACK_H

typedef int stack_t;

int get_last_item_from_stack();
int pop();
void push(int value);

#endif
