#ifndef YEMU_OCPU_H
#define YEMU_OCPU_H

#include <stdio.h>
#include <yemu/common.h>

#define MAX_MEMORY 1024*64

// Registers

#define REG_AL 0xA0
#define REG_AH 0xA1
#define REG_A 0x0A

#define REG_BL 0xB0
#define REG_BH 0xB1
#define REG_B 0x0B

#define REG_CL 0xC0
#define REG_CH 0xC1
#define REG_C 0x0C

#define REG_DL 0xD0
#define REG_DH 0xD1
#define REG_D 0x0D

#define REG_EL 0xE0
#define REG_EH 0xE1
#define REG_E 0x0E

#define REG_FL 0xF0
#define REG_FH 0xF1
#define REG_F 0x0F

// Opcodes

#define INS_MOV_IM 0x01
#define INS_MOV_REG 0xA1
#define INS_ADD_IM 0x10
#define INS_ADD_REG 0xB0
#define INS_ADC_IM 0x11
#define INS_ADC_REG 0xB1
#define INS_SUB_IM 0x12
#define INS_SUB_REG 0xB2
#define INS_MUL_IM 0x13
#define INS_MUL_REG 0xB3
#define INS_DIV_IM 0x14
#define INS_DIV_REG 0xB4
#define INS_INC 0x15
#define INS_DEC 0x16
#define INS_NOT 0x20
#define INS_AND_IM 0x21
#define INS_AND_REG 0xC1
#define INS_OR_IM 0x22
#define INS_OR_REG 0xC2
#define INS_NOR_IM 0x23
#define INS_NOR_REG 0xC3
#define INS_XOR_IM 0x24
#define INS_XOR_REG 0xC4
#define INS_NAND_IM 0x25
#define INS_NAND_REG 0xC5
#define INS_CMP_IM 0x26
#define INS_CMP_REG 0xC6
#define INS_PUSH_IM 0x30
#define INS_POP 0x31
#define INS_OCPU_NOP 0x90
#define INS_OCPU_SEC 0x61
#define INS_OCPU_CLC 0x62

struct OCPU_MEMORY {
  byte memory[MAX_MEMORY];
};

struct OCPU {
  word PC; // The program counter is a 16 bit register which points to the next instruction to be executed.
  byte SP; // The stack pointer is an 8 bit register and holds the low 8 bits of the next free location on the stack.

  byte AL;
  byte AH;
  word A;

  byte BL;
  byte BH;
  word B;

  byte CL;
  byte CH;
  word C;

  byte DL;
  byte DH;
  word D;
  
  byte EL;
  byte EH;
  word E;

  byte FL;
  byte FH;
  word F;
  
  // Flags (or Processor Status)
  byte	ZF; // Zero Flag
  byte	NF; // Negative Flag
  byte 	CF; // Carry Flag
  byte  OF; // Overflow Flag
};

void initOCPU(FILE *infile);

#endif
