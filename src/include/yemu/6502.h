#ifndef YEMU_6502_H
#define YEMU_6502_H

#include <stdio.h>
#include <yemu/common.h>

#define MAX_MEMORY 1024*64

// Opcodes

#define INS_LDA_IM 0xA9 // LDA Immediate
#define INS_LDA_ZP 0xA5 // LDA Zero Page
#define INS_LDA_ZPX 0xB5 // LDA Zero Page,X
#define INS_LDA_ABS 0xAD // LDA Absolute
#define INS_LDA_ABSX 0xBD // LDA Absolute,X
#define INS_LDA_ABSY 0xB9 // LDA Absolute,Y
#define INS_LDA_INX 0xA1 // LDA Indirect,X
#define INS_LDA_INY 0xB1 // LDA Indirect,Y

#define INS_LDX_IM 0xA2 // LDX Immediate
#define INS_LDX_ZP 0xA6 // LDX Zero Page
#define INS_LDX_ZPY 0xB6 // LDX Zero Page,Y
#define INS_LDX_ABS 0xAE // LDX Absolute
#define INS_LDX_ABSY 0xBE // LDX Absolute,Y

#define INS_LDY_IM 0xA0 // LDY Immediate
#define INS_LDY_ZP 0xA4 // LDY Zero Page
#define INS_LDY_ZPX 0xB4 // LDY Zero Page,X
#define INS_LDY_ABS 0xAC // LDY Absolute
#define INS_LDY_ABSX 0xBC // LDY Absolute,X

#define INS_TAX 0xAA // TAX Implied
#define INS_TAY 0xA8 // TAY Implied
#define INS_TSX 0xBA // TSX Implied
#define INS_TXA 0x8A // TXA Implied
#define INS_TXS 0x9A // TXS Implied
#define INS_TYA 0x98 // TYA Implied

#define INS_JSR 0x20 // JSR
#define INS_RTS 0x60 // RTS

#define INS_6502_NOP 0xEA // NOP

#define INS_INX 0xE8 // INX Implied
#define INS_INY 0xC8 // INY Implied

#define INS_DEX 0xCA // DEX Implied
#define INS_DEY 0x88 // DEY Implied

struct MEMORY {
	byte memory[MAX_MEMORY];
};

struct CPU {
	word 	PC; // The program counter is a 16 bit register which points to the next instruction to be executed.
	byte 	SP; // The stack pointer is an 8 bit register and holds the low 8 bits of the next free location on the stack.

	byte	A;
	byte	X;
	byte	Y;

	// Flags (or Processor Status)
	byte	C; // Carry Flag
	byte	Z; // Zero Flag
	byte 	I; // Interrupt Disable
	byte	D; // Decimal Mode
	byte	B; // Break Command
	byte	V; // Overflow Flag
	byte	N; // Negative Flag
};

void init6502(FILE *infile);

#endif
